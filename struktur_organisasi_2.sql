# QUERY MENCARI CEO
SELECT * FROM employee AS CEO
WHERE ISNULL(atasan_id);

# QUERY MENCARI DIREKTUR
SELECT * FROM employee AS direktur
WHERE atasan_id = (
    SELECT id FROM employee AS CEO
    WHERE isnull(atasan_id));

# QUERY MENCARI MANAGER
SELECT * FROM employee AS manager
WHERE atasan_id IN (
        SELECT id FROM employee AS direktur
        WHERE atasan_id = (
            SELECT id FROM employee AS CEO
            WHERE isnull(atasan_id)));

# QUERY MENCARI STAFF BIASA
SELECT * FROM employee AS staff
WHERE atasan_id IN (
    SELECT id FROM employee AS manager
    WHERE atasan_id IN (
        SELECT id FROM employee AS direktur
        WHERE atasan_id = (
            SELECT id FROM employee AS CEO
            WHERE isnull(atasan_id))));

# MNECARI BAWAHAN PAK BUDI
SELECT COUNT(*) FROM employee
WHERE atasan_id IN (
    SELECT id FROM employee
    WHERE nama = 'Pak Budi'
    );

# MENCARI BAWAHAN PAK TONO
SELECT COUNT(*) AS BAWAHAN_PAK_TONO FROM employee
WHERE atasan_id = (
    SELECT id FROM employee
    WHERE nama = 'Pak Tono'
    );

# MENCARI BAWAHAN PAK TOTOK
SELECT COUNT(*) AS BAWAHAN_PAK_TOTOK FROM employee
WHERE atasan_id = (
    SELECT id FROM employee
    WHERE nama = 'Pak Totok'
    );

# MENCARI BAWAHAN BU SINTA
SELECT COUNT(*) AS BAWAHAN_BU_SINTA FROM employee
WHERE atasan_id = (
    SELECT id FROM employee
    WHERE nama = 'Bu Sinta'
    );

# MENCARI BAWAHAN BU NOVI
SELECT COUNT(*) AS BAWAHAN_BU_NOVI FROM employee
WHERE atasan_id = (
    SELECT id FROM employee
    WHERE nama = 'Bu Novi'
    );